package game2017;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
	private ArrayList<Connection> playersInGame;
	private ServerSocket listenSocket;
	private String[] board = { // 20x20
			"wwwwwwwwwwwwwwwwwwww",
			"w        ww        w",
			"w w  w  www w  w  ww",
			"w w  w   ww w  w  ww",
			"w  w               w",
			"w w w w w w w  w  ww",
			"w w     www w  w  ww",
			"w w     w w w  w  ww",
			"w   w w  w  w  w   w",
			"w     w  w  w  w   w",
			"w ww ww        w  ww",
			"w  w w    w    w  ww",
			"w        ww w  w  ww",
			"w         w w  w  ww",
			"w        w     w  ww",
			"w  w              ww",
			"w  w www  w w  ww ww",
			"w w      ww w     ww",
			"w   w   ww  w      w",
			"wwwwwwwwwwwwwwwwwwww" };

	public Server() {
		playersInGame = new ArrayList<Connection>();
		try {
			listenSocket = new ServerSocket(6789);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Thread accept = new Thread() {
			@Override
			public void run() {
				while (true) {
					try {
						Socket connectionSocket = listenSocket.accept();
						Connection con = new Connection(connectionSocket);
						playersInGame.add(con);
						con.start();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		};
		accept.start();
	}

	public static void main(String[] args) throws Exception {
		new Server();
	}

	public String[] boardOccupied() {
		String[] boardOccupied = board.clone();
		for (Connection c : playersInGame) {
			String playerLine = boardOccupied[c.player.getYpos()];
			String newBoardLine = playerLine.substring(0, c.player.getXpos()) + "p"
					+ playerLine.substring(c.player.getXpos() + 1);
			boardOccupied[c.player.getYpos()] = newBoardLine;
		}
		return boardOccupied;
	}

	public int[] getStartPos() {
		int[] startPos = { 0, 0 };
		boolean freePosFound = false;

		while (!freePosFound) {
			int randomX = (int) Math.ceil(Math.random() * board[0].length() - 1);
			int randomY = (int) Math.ceil(Math.random() * board.length - 1);
			if (boardOccupied()[randomY].charAt(randomX) == ' ') {
				startPos[1] = randomY;
				startPos[0] = randomX;
				freePosFound = true;
				return startPos;
			}
		}
		//		for (int y = 0; y < boardOccupied().length; y++) {
		//			for (int x = 0; x < boardOccupied()[y].length(); y++) {
		//				if (boardOccupied()[y].charAt(x) == ' ') {
		//					startPos[1] = y;
		//					startPos[0] = x;
		//					return startPos;
		//				}
		//			}
		//		}
		return null;
	}

	private class Connection extends Thread {
		private Socket clientSocket;
		private ObjectInputStream conInputStream;
		private ObjectOutputStream conOutputStream;
		private Player player = null;
		private boolean boardSent = false;

		Connection(Socket socket) throws IOException {
			this.clientSocket = socket;
			this.conInputStream = new ObjectInputStream(clientSocket.getInputStream());
			this.conOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
		}

		@Override
		public void run() {
			while (true) {
				try {
					if (!boardSent) {
						conOutputStream.writeObject(board);
						conOutputStream.reset();
						boardSent = true;
					}
					Object o = conInputStream.readObject();
					System.out.println("getting request from client");
					if (o instanceof Player) {
						Player p = (Player) o;
						if (player == null) {
							this.player = p;
							int[] startPosition = getStartPos();
							int startX = startPosition[0];
							int startY = startPosition[1];
							System.out.println(getStartPos());
							System.out.println(startX);
							System.out.println(startY);
							p.setXpos(startX);
							p.setYpos(startY);
						}
						sendToClients(p);
					} else if (o instanceof String) {
						String direction = (String) o;
						System.out.println("request was a direction, " + direction);
						int delta_x;
						int delta_y;
						switch (direction) {
						case "up":
							delta_x = 0;
							delta_y = -1;
							break;
						case "down":
							delta_x = 0;
							delta_y = 1;
							break;
						case "left":
							delta_x = -1;
							delta_y = 0;
							break;
						case "right":
							delta_x = 1;
							delta_y = 0;
							break;
						default:
							delta_x = 0;
							delta_y = 0;
							break;
						}
						sendToClients(this.player, delta_x, delta_y, direction);
						//						System.out.println(getStartPos());
						//						for (int i = 0; i < boardOccupied().length; i++) {
						//							System.out.println(boardOccupied()[i]);
						//						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		public void write(Player p) {
			try {
				conOutputStream.writeObject(p);
				conOutputStream.reset();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void sendToClients(Player p) {
		//		long ran = (long) Math.ceil((Math.random() * 2000));
		//		TimeUnit.MILLISECONDS.sleep(ran);
		System.out.println("updating clients!");
		for (Connection c : playersInGame) {
			c.write(p);
		}
	}

	public synchronized void sendToClients(Player p, int delta_x, int delta_y, String direction) {
		System.out.println("processing movement request from client");
		p.setDirection(direction);
		int x = p.getXpos();
		int y = p.getYpos();

		if (board[y + delta_y].charAt(x + delta_x) == 'w') {
			p.addPoints(-1);
		} else {
			Player otherPlayer = getPlayerAt(x + delta_x, y + delta_y);
			if (otherPlayer != null) {
				p.addPoints(10);
				otherPlayer.addPoints(-10);
				for (Connection c : playersInGame) {
					c.write(otherPlayer);
				}
			} else {
				p.addPoints(1);
				x += delta_x;
				y += delta_y;
				p.setXpos(x);
				p.setYpos(y);
			}
		}
		sendToClients(p);
	}

	public Player getPlayerAt(int x, int y) {
		for (Connection c : playersInGame) {
			if (c.player.getXpos() == x && c.player.getYpos() == y) {
				return c.player;
			}
		}
		return null;
	}

}