package game2017;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Client2 extends Application {

	public Socket clientSocket;
	public String serverIP = "localhost"; // "10.10.132.50"
	public String playerName = "Klient 2";
	//	public int playerStartX = 6;
	//	public int playerStartY = 13;
	public ObjectOutputStream clientOutputStream;
	public ObjectInputStream clientInputStream;

	public static final int size = 20;
	public static final int scene_height = size * 20 + 100;
	public static final int scene_width = size * 20 + 200;

	public static Image image_floor;
	public static Image image_wall;
	public static Image my_hero_right, my_hero_left, my_hero_up, my_hero_down, hero_right, hero_left, hero_up,
			hero_down;

	public static Player me;
	public static List<Player> players = new ArrayList<Player>();

	private Label[][] fields;
	private TextArea scoreList;

	private String[] board;

	// -------------------------------------------
	// | Maze: (0,0) | Score: (1,0)              |
	// |-----------------------------------------|
	// | boardGrid (0,1) | scorelist (1,1)       |
	// |                 |                       |
	// -------------------------------------------

	@Override
	public void start(Stage primaryStage) {
		try {
			GridPane grid = new GridPane();
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(0, 10, 0, 10));

			Text mazeLabel = new Text("Maze:");
			mazeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

			Text scoreLabel = new Text("Score:");
			scoreLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

			scoreList = new TextArea();

			GridPane boardGrid = new GridPane();

			image_wall = new Image(getClass().getResourceAsStream("Image/wall4.png"), size, size, false, false);
			image_floor = new Image(getClass().getResourceAsStream("Image/floor1.png"), size, size, false, false);

			hero_right = new Image(getClass().getResourceAsStream("Image/heroRight.png"), size, size, false, false);
			hero_left = new Image(getClass().getResourceAsStream("Image/heroLeft.png"), size, size, false, false);
			hero_up = new Image(getClass().getResourceAsStream("Image/heroUp.png"), size, size, false, false);
			hero_down = new Image(getClass().getResourceAsStream("Image/heroDown.png"), size, size, false, false);
			my_hero_right = new Image(getClass().getResourceAsStream("Image/myHeroRight.png"), size, size, false,
					false);
			my_hero_left = new Image(getClass().getResourceAsStream("Image/myHeroLeft.png"), size, size, false, false);
			my_hero_up = new Image(getClass().getResourceAsStream("Image/myHeroUp.png"), size, size, false, false);
			my_hero_down = new Image(getClass().getResourceAsStream("Image/myHeroDown.png"), size, size, false, false);

			clientSocket = new Socket(serverIP, 6789);
			clientOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
			clientInputStream = new ObjectInputStream(clientSocket.getInputStream());
			board = getBoardFromServer();

			fields = new Label[20][20];
			for (int j = 0; j < 20; j++) {
				for (int i = 0; i < 20; i++) {
					switch (board[j].charAt(i)) {
					case 'w':
						fields[i][j] = new Label("", new ImageView(image_wall));
						break;
					case ' ':
						fields[i][j] = new Label("", new ImageView(image_floor));
						break;
					default:
						throw new Exception("Illegal field value: " + board[j].charAt(i));
					}
					boardGrid.add(fields[i][j], i, j);
				}
			}
			scoreList.setEditable(false);

			grid.add(mazeLabel, 0, 0);
			grid.add(scoreLabel, 1, 0);
			grid.add(boardGrid, 0, 1);
			grid.add(scoreList, 1, 1);

			Scene scene = new Scene(grid, scene_width, scene_height);
			primaryStage.setScene(scene);
			primaryStage.show();

			ArrayList<Player> previousPlayerInfo = new ArrayList<>();

			Thread read = new Thread() {
				Player playerFromServer = null;

				@Override
				public void run() {
					while (true) {
						try {

							for (Player p : players) {
								previousPlayerInfo.add(p);
							}

							boolean newPlayer = true;
							Object o = clientInputStream.readObject();
							if (o instanceof Player) {
								playerFromServer = (Player) o;
							}
							for (Player p : players) {
								if (playerFromServer.getName().equalsIgnoreCase(p.getName())) {
									players.set(players.indexOf(p), playerFromServer);
									newPlayer = false;
								}
							}

							if (playerFromServer.getName().equalsIgnoreCase(me.getName())) {
								me.setPoint(playerFromServer.getPoint());
								me.setXpos(playerFromServer.getXpos());
								me.setYpos(playerFromServer.getYpos());
								me.setDirection(playerFromServer.getDirection());
							}

							if (newPlayer) {
								players.add(playerFromServer);
							}

							for (Player p : previousPlayerInfo) {
								if (playerFromServer.getName().equalsIgnoreCase(p.getName())) {
									int prevX = p.getXpos();
									int prevY = p.getYpos();

									if (prevX != playerFromServer.getXpos() || prevY != playerFromServer.getYpos()) {
										Platform.runLater(new Runnable() {
											@Override
											public void run() {
												fields[prevX][prevY].setGraphic(new ImageView(image_floor));
											}
										});
									}
								}
							}

							Platform.runLater(new Runnable() {
								@Override
								public void run() {
									switch (playerFromServer.getDirection()) {
									case "up":
										if (playerFromServer.getName().equals(me.getName())) {
											fields[playerFromServer.getXpos()][playerFromServer.getYpos()]
													.setGraphic(new ImageView(my_hero_up));
										} else {
											fields[playerFromServer.getXpos()][playerFromServer.getYpos()]
													.setGraphic(new ImageView(hero_up));
										}
										break;
									case "down":
										if (playerFromServer.getName().equals(me.getName())) {
											fields[playerFromServer.getXpos()][playerFromServer.getYpos()]
													.setGraphic(new ImageView(my_hero_down));
										} else {
											fields[playerFromServer.getXpos()][playerFromServer.getYpos()]
													.setGraphic(new ImageView(hero_down));
										}
										break;
									case "left":
										if (playerFromServer.getName().equals(me.getName())) {
											fields[playerFromServer.getXpos()][playerFromServer.getYpos()]
													.setGraphic(new ImageView(my_hero_left));
										} else {
											fields[playerFromServer.getXpos()][playerFromServer.getYpos()]
													.setGraphic(new ImageView(hero_left));
										}
										break;
									case "right":
										if (playerFromServer.getName().equals(me.getName())) {
											fields[playerFromServer.getXpos()][playerFromServer.getYpos()]
													.setGraphic(new ImageView(my_hero_right));
										} else {
											fields[playerFromServer.getXpos()][playerFromServer.getYpos()]
													.setGraphic(new ImageView(hero_right));
										}
										break;
									}
								}
							});

							previousPlayerInfo.clear();

							scoreList.setText(getScoreList());
						} catch (ClassNotFoundException | IOException e) {
							e.printStackTrace();
						}
					}
				}
			};
			read.start();

			Thread keyListener = new Thread() {

				@Override
				public void run() {
					scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
						switch (event.getCode()) {
						case UP:
							writeToServer("up");
							break;
						case DOWN:
							writeToServer("down");
							break;
						case LEFT:
							writeToServer("left");
							break;
						case RIGHT:
							writeToServer("right");
							break;
						case SPACE:
							writeToServer(me);
						default:
							break;
						}
					});
				}
			};
			keyListener.start();

			// Setting up players
			if (me == null) {
				me = new Player(playerName, 0, 0, "up");
				players.add(me);
				writeToServer(me);
			}

			scoreList.setText(getScoreList());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	public String getScoreList() {
		StringBuffer b = new StringBuffer(10000);
		for (Player p : players) {
			b.append(p + "\r\n");
		}
		return b.toString();
	}

	public String[] getBoardFromServer() throws ClassNotFoundException, IOException {
		String[] boardFromServer = { "" };
		Object o = clientInputStream.readObject();
		if (o instanceof String[]) {
			boardFromServer = (String[]) o;
		}
		return boardFromServer;
	}

	public void writeToServer(Player p) {
		try {
			clientOutputStream.writeObject(p);
			clientOutputStream.reset();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeToServer(String direction) {
		System.out.println("sending movement request to server");
		try {
			clientOutputStream.writeObject(direction);
			clientOutputStream.reset();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
